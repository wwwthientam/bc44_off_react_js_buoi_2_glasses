import React, { Component } from "react";

export default class Detail extends Component {
  render() {
    let { url, price, name, desc } = this.props.detail;
    return (
      <div className="item_change" style={{ position: "absolute" }}>
        <div>
          {" "}
          <img src={url} />
        </div>
        <div id="detail">
          <h2>Name: {name}</h2>
          <p>Price: {price}</p>
          <p>Description: {desc}</p>
        </div>
      </div>
    );
  }
}
