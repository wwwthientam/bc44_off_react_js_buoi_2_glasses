import React, { Component } from "react";
import { GlassesArr } from "./Data";
import Detail from "./Detail";
import ListGlasses from "./ListGlasses";
export default class Ex_Glasses extends Component {
  state = {
    glassesArr: GlassesArr,
    detail: GlassesArr[0],
  };
  handleChangeGlasses = (glasses) => {
    this.setState({
      detail: glasses,
    });
  };
  render() {
    return (
      <div id="Glasses">
        <div class="container">
          <h1 className="text-center text-success p-5">Try App Glasses</h1>
          <div className="row pb-5">
            <div className="col-6 text-center">
              <h2>BEFORE</h2>
              <img src="./glassesImage/model.jpg" alt="" />
            </div>
            <div className="col-6 text-center">
              <h2>AFTER</h2>
              <Detail detail={this.state.detail} />
              <img src="./glassesImage/model.jpg" alt="" />
            </div>
          </div>
          <h1 className="text-center p-3">CHANGE OPTIONS</h1>
          <ListGlasses
            GlassesArr={this.state.glassesArr}
            handleChangeGlasses={this.handleChangeGlasses}
          />
        </div>
      </div>
    );
  }
}
