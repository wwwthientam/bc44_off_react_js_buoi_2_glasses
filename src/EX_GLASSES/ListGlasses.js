import React, { Component } from "react";
import ItemGlasses from "./ItemGlasses";
export default class ListGlasses extends Component {
  renderListGlasses() {
    return this.props.GlassesArr.map((glasses, index) => {
      return (
        <ItemGlasses
          data={glasses}
          ket={index}
          handleChangeGlasses={this.props.handleChangeGlasses}
        />
      );
    });
  }
  render() {
    return <div className="row">{this.renderListGlasses()}</div>;
  }
}
